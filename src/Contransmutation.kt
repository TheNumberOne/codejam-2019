import java.util.*

fun main() {
    val t = readLine()!!.toInt()
    for (i in 1..t) {
        val m = readLine()!!.toInt()
        val reactions = (1..m).map { metal ->
            val (p1, p2) = readLine()!!.split(" ").map { it.toInt() }
            metal to Reaction(p1, p2)
        }.toMap()
        val startMetals = readLine()!!.split(" ").mapIndexed { j, s -> (j + 1) to s.toInt() }.toMap()
        val result = countEndLeads(reactions, startMetals)
        println("Case #$i: ${result ?: "UNBOUNDED"}")
    }
}

data class Reaction(val product1: Int, val product2: Int)

fun countEndLeads(reactions: Map<Int, Reaction>, startMetals: Map<Int, Int>): Long? {
    val consideredMetals =
        getPossibleResults(
            reactions,
            startMetals.filter { it.value > 0 }.map { it.key }
        ) intersect
                getPossibleParents(
                    reactions,
                    listOf(1)
                )
    return null
}

fun getPossibleResults(reactions: Map<Int, Reaction>, metals: List<Int>): Set<Int> {
    val processed = mutableSetOf<Int>()
    val toProcess = Stack<Int>()
    toProcess.addAll(metals)
    while (!toProcess.isEmpty()) {
        val top = toProcess.pop()
        if (!processed.add(top)) continue
        toProcess.push(reactions.getValue(top).product1)
        toProcess.push(reactions.getValue(top).product2)
    }
    return processed
}

fun getPossibleParents(reactions: Map<Int, Reaction>, metals: List<Int>): Set<Int> {
    val reversed = reactions.flatMap { (m, reaction) -> listOf(reaction.product1 to m, reaction.product2 to m) }
        .groupBy({ it.first }, { it.second })
    val processed = mutableSetOf<Int>()
    val toProcess = Stack<Int>()
    toProcess.addAll(metals)
    while (!toProcess.isEmpty()) {
        val top = toProcess.pop()
        if (!processed.add(top)) continue
        for (parent in reversed.getValue(top)) {
            toProcess.push(parent)
        }
    }
    return processed
}