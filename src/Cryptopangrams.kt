import java.math.BigInteger

fun main() {
    val t = readLine()!!.toInt()
    for (i in 1..t) {
        val (n, l) = readLine()!!.split(' ').map { it.toBigInteger() }
        val list = readLine()!!.split(' ').map { it.toBigInteger() }
        println("Case #$i: ${solve(n, list)}")
    }
}

fun solve(n: BigInteger, encoded: List<BigInteger>): String {
    val primes = (0..encoded.size).map { BigInteger.ZERO }.toMutableList()

    var middle: Int = -1
    for ((i, p) in encoded.zipWithNext().withIndex()) {
        val (a, b) = p
        if (a == b) continue
        primes[i + 1] = gcd(a, b)
        middle = i + 1
        break
    }

    if (middle == -1) {
        for ((i, product) in encoded.withIndex()) {
            val s = product.sqrt()
            if (s * s == product) {
                primes[i] = s
                middle = i
            }
        }
    }

    if (middle == -1) error("Unsolvable")

    for (i in (middle + 1)..encoded.size) {
        primes[i] = encoded[i - 1] / primes[i - 1]
    }
    for (i in (middle - 1) downTo 0) {
        primes[i] = encoded[i] / primes[i + 1]
    }

    val codeBook = primes.toSet().sorted().mapIndexed { i, p -> p to ('A' + i) }.toMap()
    if (codeBook.size != 26) error("Unsolvable")
    return primes.map { codeBook.getValue(it) }.joinToString(separator = "")
}

fun gcd(a1: BigInteger, b1: BigInteger): BigInteger {
    var a = a1
    var b = b1
    while (b != BigInteger.ZERO) {
        val t = b
        b = a % b
        a = t
    }
    return a
}

//https://gist.github.com/JochemKuijpers/cd1ad9ec23d6d90959c549de5892d6cb
fun BigInteger.sqrt(): BigInteger {
    var a = BigInteger.ONE
    var b = (this shr 5) + 8.toBigInteger()
    while (b >= a) {
        val mid = (a + b) shr 1
        if (mid * mid > this) {
            b = mid - BigInteger.ONE
        } else {
            a = mid + BigInteger.ONE
        }
    }
    return a - BigInteger.ONE
}