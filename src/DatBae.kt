fun main() {
    val t = readLine()!!.toInt()
    for (i in 1..t) {
        val line = readLine()
        val (n, b, f) = line!!.split(' ').map { it.toInt() }
        val s = solution(n, b, f)
        println(s.joinToString(separator = " "))
        readLine() // skip past one
    }
}

fun solution(n: Int, b: Int, f: Int): List<Int> {
    // First send messages of alternating sequences of b+1 size to pinpoint general locations
    val chunkSize = b + 1
    var message = (0 until n).map {
        if (it % (2 * chunkSize) < chunkSize) {
            '0'
        } else {
            '1'
        }
    }.joinToString(separator = "")
    println(message)
    val ret = readLine()!!
    val chunks = ret.split(Regex("(?<=0)(?=1)|(?<=1)(?=0)")).map { it.length }
    var i = 1
    val returnMessages = mutableListOf<String>()
    while (i < chunkSize) {
        val messageChunk = (0 until chunkSize).map {
            if (it % (2 * i) < i) {
                '0'
            } else {
                '1'
            }
        }.joinToString(separator = "")
        message = (0 until n).map { messageChunk[it % chunkSize] }.joinToString(separator = "")
        println(message)
        returnMessages += readLine()!!
        i *= 2
    }
    returnMessages.reverse()
    val chunkedWorkingIndices = (0 until (n - b)).map { i ->
        var chunkIndex = 0
        for (s in returnMessages) {
            chunkIndex = chunkIndex * 2 + if (s[i] == '0') 0 else 1
        }
        chunkIndex
    }
    val workingIndices = mutableSetOf<Int>()
    var beforeIndex = 0
    var afterIndex = 0
    for (chunk in chunks) {
        for (j in 0 until chunk) {
            workingIndices += afterIndex + chunkedWorkingIndices[beforeIndex + j]
        }
        beforeIndex += chunk
        afterIndex += chunkSize
    }
    return ((0 until n).toSet() - workingIndices).sorted()
}