import kotlin.system.exitProcess

fun main() {
    val (t, w) = readLine()!!.split(" ").map { it.toInt() }
    for (n in 1..t) {
        play(w)
    }
}

fun play(w: Int) {
    println(210)
    var numRings = readLine()!!.toLong()
    if (numRings == -1L) exitProcess(1)
    val r4 = numRings shr 52
    val r5 = numRings shr 42 and 0x7F
    val r6 = numRings shr 35 and 0x7F

    println(38)
    numRings = readLine()!!.toLong()
    if (numRings == -1L) exitProcess(1)
    numRings -= r4 * (1 shl 9)
    numRings -= r5 * (1 shl 7)
    numRings -= r6 * (1 shl 6)
    val r1 = numRings shr 38
    val r2 = numRings shr 19 and 0x7F
    val r3 = numRings shr 12 and 0x7F

    println(listOf(r1, r2, r3, r4, r5, r6).joinToString(" "))
    if (readLine() == "-1") {
        exitProcess(1)
    }
}
