import kotlin.math.abs
import kotlin.math.max
import kotlin.random.Random
import kotlin.system.measureNanoTime
import kotlin.system.measureTimeMillis

fun main() {
    val t = readLine()!!.toInt()
    for (i in 1..t) {
        val (n, k) = readLine()!!.split(" ").map { it.toInt() }
        val c = readLine()!!.split(" ").map { it.toInt() }
        val d = readLine()!!.split(" ").map { it.toInt() }
        val result = numChoices(n, k, c, d)
        println("Case #$i: $result")
    }
    val c = List(100000) { Random.nextInt(100) }
    val d = List(100000) { Random.nextInt(100) }
    var r: Long = numChoices(100000, 20, c, d)
    val time = measureTimeMillis {
        r = numChoices(100000, 20, c, d)
    }
    println("$r $time")
}

fun numChoices(n: Int, k: Int, c: List<Int>, d: List<Int>): Long {
    var choices = 0L
    for (l in 0 until n) {
        var maxC = c[l]
        var maxD = d[l]
        for (r in l until n) {
            maxC = max(maxC, c[r])
            maxD = max(maxD, d[r])
            if (abs(maxC - maxD) <= k) {
                choices++
            }
        }
    }
    return choices
}
