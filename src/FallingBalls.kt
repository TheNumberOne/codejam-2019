import kotlin.math.abs

fun main() {
    val t = readLine()!!.toInt()
    for (i in 1..t) {
        val c = readLine()!!.toInt()
        val b = readLine()!!.split(" ").map { it.toInt() }
        val result = findRamps(c, b)
        print("Case #$i: ")
        result.print()
    }
}

sealed class Result {
    abstract fun print()

    object Impossible : Result() {
        override fun print() {
            println("IMPOSSIBLE")
        }
    }

    class Ramps(private val rows: List<String>) : Result() {
        override fun print() {
            println(rows.size)
            for (row in rows) {
                println(row)
            }
        }
    }
}

fun findRamps(c: Int, lastRow: List<Int>): Result {
    if (lastRow[0] == 0 || lastRow.last() == 0) {
        return Result.Impossible
    }

    val to = MutableList(c) { 0 }
    run {
        var i = 0
        for ((j, numBalls) in lastRow.withIndex()) {
            repeat(numBalls) {
                to[i++] = j
            }
        }
    }

    val moved = List(c) { i -> to[i] - i }
    if (moved[0] != 0 || moved.last() != 0) {
        return Result.Impossible
    }

    val numRows = moved.map { abs(it) }.max()!! + 1
    val columns = List(c) { mutableSetOf<Int>() }

    for ((i, m) in moved.withIndex()) {
        if (m == 0) continue
        for (j in if (m > 0) 0 until m else 0 downTo m + 1) {
            columns[i + j] += m - j
        }
    }

    val rows = List(numRows) { r ->
        columns.map { if (it.contains(r)) '\\' else if (it.contains(-r)) '/' else '.' }.joinToString("")
    }.reversed()

    return Result.Ramps(rows)
}

