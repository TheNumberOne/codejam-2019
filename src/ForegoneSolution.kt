
fun main(){
    val t = readLine()!!.toInt()
    for (i in 1..t) {
        val n = readLine()!!
        val (a, b) = split(n)
        println("Case #$i: $a $b")
    }
}

fun split(n: String): Pair<String, String> {
    val a = n.replace('4', '2')
    val b = n.replace(Regex("[0-35-9]"), "0").replace('4', '2').trimStart('0')
    return Pair(a, b)
}