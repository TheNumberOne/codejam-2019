import kotlin.math.max

fun main() {
    val t = readLine()!!.toInt()
    for (i in 1..t) {
        val (r, b) = readLine()!!.split(" ").map { it.toInt() }
        val jugglers = findMaxJugglers(r, b, emptySet(), setOf(1 to 0, 0 to 1))
        println("Case #$i: $jugglers")
    }
}

fun findMaxJugglers(r: Int, b: Int, usedSaws: Set<Pair<Int, Int>>, candidateSaws: Set<Pair<Int, Int>>): Int {
    if (candidateSaws.isEmpty()) {
        println(usedSaws.size)
        return usedSaws.size
    }

    val candidate = candidateSaws.first()
    val (cR, cB) = candidate
    var best = 0
    if (cR <= r && cB <= b) {
        val newCandSaws = candidateSaws.toMutableSet()
        val newUsed = usedSaws.toMutableSet()
        newUsed += candidate
        newCandSaws -= candidate
        if (cB == 0 || usedSaws.contains(cR + 1 to cB - 1)) {
            newCandSaws += cR + 1 to cB
        }
        if (cR == 0 || usedSaws.contains(cR - 1 to cB + 1)) {
            newCandSaws += cR to cB + 1
        }
        // recurse
        best = findMaxJugglers(r - cR, b - cB, newUsed, newCandSaws)
    }
    // recurse
    return max(best, findMaxJugglers(r, b, usedSaws, candidateSaws - candidate))
}
