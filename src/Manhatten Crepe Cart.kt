data class Coord(val x: Int, val y: Int)
data class Person(val p: Coord, val dir: Char)

fun main() {
    val t = readLine()!!.toInt()
    for (n in 1..t) {
        val (p, q) = readLine()!!.split(" ").map { it.toInt() }
        val people = mutableListOf<Person>()
        repeat(p) {
            val (xStr, yStr, dStr) = readLine()!!.split(" ")
            val x = xStr.toInt()
            val y = yStr.toInt()
            val dir = dStr[0].toLowerCase()
            people += Person(Coord(x, y), dir)
        }
        val (x, y) = findCrepeCart(p, q, people)
        println("Case #$n: $x $y")
    }
}

sealed class Box(val upperLeft: Coord, val upperRight: Coord)

fun findCrepeCart(numPeople: Int, max_x: Int, people: List<Person>): Coord {
    val boxes = mutableSetOf<Box>()

    val east = people.filter { it.dir == 'e' }
    val west = people.filter { it.dir == 'w' }
    val north = people.filter { it.dir == 'n' }
    val south = people.filter { it.dir == 's' }

    val totalWest = west.size
    val eastWest = east + west
    val northSouth = north + south

    return Coord(bestCoord(totalWest, eastWest), bestCoord(south.size, northSouth))
}

private fun bestCoord(totalWest: Int, eastWest: List<Person>): Int {
    var currentIntersections = totalWest
    var bestIntersections = currentIntersections
    var bestX = 0
    for ((x, people) in eastWest.groupBy {
        when (it.dir) {
            'e' -> it.p.x + 1
            'n' -> it.p.y + 1
            'w' -> it.p.x
            's' -> it.p.y
            else -> error("")
        }
    }.toSortedMap()) {
        for (person in people) {
            currentIntersections += when (person.dir) {
                'e', 'n' -> 1
                'w', 's' -> -1
                else -> error("")
            }
        }
        if (currentIntersections > bestIntersections) {
            bestIntersections = currentIntersections
            bestX = x
        }
    }
    return bestX
}
