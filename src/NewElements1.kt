import java.lang.Math.pow
import kotlin.math.abs

fun main() {
    val t = readLine()!!.toInt()
    for (i in 1..t) {
        val n = readLine()!!.toInt()
        val molecules = (1..n).map {
            val (c, j) = readLine()!!.split(" ").map { it.toInt() }
            c to j
        }
        val result = numOrderings(molecules)
        println("Case #$i: $result")
    }
}

class Fraction(num: Long, denom: Long) {
    val numerator: Long
    val denominator: Long

    constructor(num: Int, denom: Int) : this(num.toLong(), denom.toLong())

    init {
        check(denom != 0L) { "Invalid fraction" }
        if (num == 0L) {
            numerator = 0
            denominator = 1
        } else {
            val gcd = gcd(abs(num), abs(denom))
            val sign = if ((num > 0) == (denom > 0)) 1 else -1
            numerator = sign * abs(num) / gcd
            denominator = abs(denom) / gcd
        }
    }

    operator fun compareTo(other: Fraction): Int {
        return (numerator * other.denominator).compareTo(other.numerator * denominator)
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Fraction) return false
        return numerator == other.numerator && denominator == other.denominator
    }

    override fun hashCode(): Int {
        var result = numerator.hashCode()
        result = 31 * result + denominator.hashCode()
        return result
    }
}

fun Int.toFraction() = Fraction(this, 1)

fun gcd(a: Long, b: Long): Long = if (b == 0L) a else gcd(b, a % b)

fun Long.factorial(): Long {
    check(this >= 0)
    var result = 0L
    for (i in 1L..this) result *= i
    return result
}

fun numOrderings(molecules: List<Pair<Int, Int>>): Long {
    val ratios = mutableSetOf<Fraction>()
    for ((i, m1) in molecules.withIndex()) {
        val (x1, x2) = m1
        for ((j, m2) in molecules.withIndex()) {
            if (j < i) continue

            val (x3, x4) = m2
            if (x1 == x3 || x2 == x4) continue
            val ratio = Fraction(-(x4 - x2), (x3 - x1))
            if (ratio > 0.toFraction()) ratios.add(ratio)
        }
    }
    return ratios.size + 1L
}