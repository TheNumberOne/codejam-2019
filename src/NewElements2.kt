package newelements2

import kotlin.math.abs
import kotlin.math.sign
import kotlin.random.Random
import kotlin.random.nextInt
import kotlin.random.nextLong

fun main() {
    val t = readLine()!!.toInt()
    for (i in 1..t) {
        val n = readLine()!!.toInt()
        val molecules = (1..n).map {
            val (c, j) = readLine()!!.split(" ").map { it.toLong() }
            c to j
        }
        val result = bestMolecularWeight(molecules)
        val formatted = result?.run { "$first $second" } ?: "IMPOSSIBLE"
        println("Case #$i: $formatted")
    }
}

fun runTestCases() {
    var i = 1
    while (true) {
        val result = bestMolecularWeight(generateTestCase())
        val formatted = result?.run { "$first $second" } ?: "IMPOSSIBLE"
        println("Case #$i: $formatted")
        i++
    }
}

fun generateTestCase(): List<Pair<Long, Long>> {
    val c = Random.nextLong(1..1_000_000_000L)
    val j = Random.nextLong(1..1_000_000_000L)

    val n = Random.nextInt(2..10)

    val molecules = (1..n).map {
        Random.nextLong(1..1_000_000_000L) to Random.nextLong(1..1_000_000_000L)
    }.toSet()

    return molecules.sortedBy { (ci, ji) -> ci * c + ji * j }
}

fun bestMolecularWeight(molecules: List<Pair<Long, Long>>): Pair<Long, Long>? {
    var min = Fraction.Zero
    var max = Fraction.PositiveInfinity

    for ((m1, m2) in molecules.zipWithNext()) {
        val (x1, x2) = m1
        val (x3, x4) = m2

        if (x3 == x1) {
            if (x4 <= x2) return null
            continue
        }
        val frac = Fraction(-(x4 - x2), x3 - x1)
        if (x3 - x1 > 0) {
            min = maxOf(frac, min)
        } else {
            max = minOf(frac, max)
        }
    }

    if (max <= min) {
        return null
    }

    val f = searchSternBrocot(min, max)

    return f.numerator to f.denominator
}

fun searchSternBrocot(
    min: Fraction,
    max: Fraction
): Fraction {
    check(max > min) { "Value to search for must be in a valid range" }
    check(max > Fraction.Zero) { "Can only search for positive fractions" }

    var low = Fraction.Zero
    var high = Fraction.PositiveInfinity

    while (true) {
        val middle = Fraction(
            low.numerator + high.numerator,
            low.denominator + high.denominator
        )

        if (min < middle && middle < max) {
            return middle
        } else if (max <= middle) {
            high = middle
        } else if (min >= middle) {
            low = middle
        }
    }
}

class Fraction(num: Long, denom: Long) : Comparable<Fraction> {
    companion object {
        val PositiveInfinity = Fraction(1, 0)
        val NegativeInfinity = -PositiveInfinity
        val Zero = Fraction(0, 1)
    }

    operator fun unaryMinus(): Fraction {
        return Fraction(-numerator, denominator)
    }

    val numerator: Long
    val denominator: Long

    constructor(num: Int, denom: Int) : this(num.toLong(), denom.toLong())

    init {
        when {
            denom == 0L -> {
                check(num != 0L) { "Invalid fraction" }
                numerator = num.sign.toLong()
                denominator = 0
            }
            num == 0L -> {
                numerator = 0
                denominator = 1
            }
            else -> {
                val gcd = gcd(abs(num), abs(denom))
                val sign = num.sign * denom.sign
                numerator = sign * abs(num) / gcd
                denominator = abs(denom) / gcd
            }
        }
    }

    override operator fun compareTo(other: Fraction): Int {
        // Make sure infinities are compared correctly.
        if (denominator == 0L && other.denominator == 0L)
            return numerator.compareTo(other.numerator)
        return (numerator * other.denominator).compareTo(other.numerator * denominator)
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Fraction) return false
        return numerator == other.numerator && denominator == other.denominator
    }

    override fun hashCode(): Int {
        var result = numerator.hashCode()
        result = 31 * result + denominator.hashCode()
        return result
    }

    override fun toString(): String {
        return "$numerator/$denominator"
    }
}

fun Int.toFraction() = Fraction(this, 1)

fun gcd(a: Long, b: Long): Long = if (b == 0L) a else gcd(b, a % b)

fun Long.factorial(): Long {
    check(this >= 0)
    var result = 0L
    for (i in 1L..this) result *= i
    return result
}