import kotlin.random.Random
import kotlin.random.nextInt

fun main() {
    val t = readLine()!!.toInt()
    for (i in 1..t) {
        val n = readLine()!!.toInt()
        val p = readLine()!!
//        val (n, p) = generate()
        println("Case #$i: ${findPath(n, p)}")
    }
}

fun generate(): Pair<Int, String> {
    val n = Random.nextInt(2..50000)
    val p = "E".repeat(n - 1) + "S".repeat(n - 1)
    val l = p.toList().shuffled(Random).joinToString(separator = "")
    return Pair(n, l)
}

fun findPath(n: Int, p: String): String {
    val first = opposite(p[0])
    val last = opposite(p[p.lastIndex])
    if (first != last) {
        return "$first".repeat(n - 1) + "$last".repeat(n - 1)
    }
    val crossover = p.indexOf("$first$first")
    if (crossover == -1) {
        error("Unsolvable")
    }
    val crossoverLocation = p.take(crossover).count { it == first } + 1
    return "$first".repeat(crossoverLocation) + "${opposite(first)}".repeat(n - 1) + "$last".repeat(n - 1 - crossoverLocation)
}

fun opposite(c: Char) = when (c) {
    'E' -> 'S'
    'S' -> 'E'
    else -> error("invalid")
}