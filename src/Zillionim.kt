import java.io.File

val file = File("log")

fun p(s: String) {
    file.appendText(s + "\n")
}

fun main() {
    val (t, w) = readLine()!!.split(" ").map { it.toInt() }

    for (i in 1..t) {
        p("\n$i")
        val game = Game(w)
        while (true) {
            p("reading")
            val p = readLine()!!.toLong()
            p("p: $p")
            if (p in -3..-2) {
                break
            }

            if (p == -1L) {
                return
            }
            val move = game.move(p)
            p("m: $move")
            println(move)
        }
    }
}

const val cellSize = 10_000_000_000L

class Game(val w: Int) {
    val blocks = mutableListOf(1L to cellSize * 100L + 1)

    fun doMove(move: Long): Long {
        val blockIndex = blocks.indexOfFirst { (a, b) -> a <= move && move + cellSize <= b }
        val (lower, higher) = blocks[blockIndex]
        val newBlocks = listOf(lower to move, move + cellSize to higher).filter { (a, b) -> b - a >= cellSize }
        blocks.removeAt(blockIndex)
        blocks.addAll(blockIndex, newBlocks)
        return move
    }

    fun move(opponentMove: Long): Long {
        doMove(opponentMove)

        // split or absorb
        if (blocks.size % 2 == 1) {
            val (a, b) = blocks[0]
            return when {
                b - a < 2 * cellSize -> doMove(a)
                b - a < 3 * cellSize -> doMove(a + cellSize - 1)
                else -> doMove(a + cellSize)
            }
        }

        // remove piece from one
        for ((a, b) in blocks) {
            // want to make sure number of cells is 1 mod 3
            val cells = (b - a) / cellSize
            val toRemove = (cells - 1) % 3

            if (toRemove > 0) {
                return if (toRemove == 1L) {
                    doMove(a)
                } else {
                    doMove(a + cellSize - 1)
                }
            }
        }

        // oh no
        val (a, _) = blocks.maxBy { (a, b) -> b - a }!!
        return doMove(a)
    }
}
